import React from "react";

import '../css/tombstone.css';
import cross from '../img/cross-solid.svg';

const Tombstone = ({ name, birthDate, deathDate, level }) => {
    return (
        <>
            <div className="tombstone-container">
                <img src={cross}  alt={"icon"}/>
                
                <div>
                    <div className="tombstone-engravings">
                        <h1 className='longname'>{name}</h1>
                        
                        <p>{`${birthDate} - ${deathDate}`}</p>
                        
                        <p>{`Niv. ${level}`}</p>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Tombstone;
