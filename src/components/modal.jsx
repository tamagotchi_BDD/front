import React from 'react';
import { Link } from 'react-router-dom';

import '../css/modal.css';

const modal = () => {
    return (
        <div className='modal'>
            <h1>Décès</h1>

            <p className='p-modal'>Votre Tamagotchi vient de mourir.</p>

            <Link to={{ pathname: '/graveyard' }} style={{ textDecoration: 'underline' }}><p className='p-modal'>Rendez-vous au Cimetière !</p></Link>
        </div>
    )
}

export default modal;