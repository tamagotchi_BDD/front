import '../css/cardTamagotchi.css';
import chevron from '../img/chevron-right-solid.svg';

const cardTamagotchi = ({ name, lvl, eat, drink, sleep, tired, user }) => {
    return (
        <div className='card'>
            <div className='subcard'>
                <div className='flex'>
                    <h2>{name}</h2>

                    <p><u>Level : {lvl}</u></p>
                </div>
                
                    <div className='flex'>
                        <div>
                            <p>Nourrir : {eat}</p>

                            <p>Boire : {drink}</p>
                        </div>

                        <div>
                            <p>Coucher : {sleep}</p>

                            <p>Ennui : {tired}</p>
                        </div>
                    </div>
                </div>

            <img src={chevron}  alt={"icon"}/>
        </div>
    )
}

export default cardTamagotchi;
