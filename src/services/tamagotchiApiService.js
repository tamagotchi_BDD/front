import axios from 'axios';

const URL = `${process.env.REACT_APP_API_PROTOCOL}://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}${process.env.REACT_APP_API_ROOT}`;

const apiService = axios.create({
    withCredentials: true,
    baseURL: URL,
    timeout: 10000,
    headers: {
        'Content-Type': 'application/json'
    }
});

export const tamagotchiApiService = apiService;
