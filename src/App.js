import * as React from "react";
import { Routes, Route } from "react-router-dom";
import { BrowserRouter } from "react-router-dom";

import "./App.css";

import Launcher from "./controller/launcher";
import Users from "./controller/users/users";
import Tamagotchis from './controller/tamagotchis/tamagotchis';
import UpdateTamagotchi from "./controller/tamagotchis/updateTamagotchi";
import Graveyard from "./controller/graveyards/graveyard";

function App() {
  return (
    <div className="app">
      <div className="game">
        <div className="app-routes">
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<Launcher />} />
              <Route path="/users" element={<Users />} />
              <Route path='/tamagotchis' element={<Tamagotchis />} />
              <Route path='/tamagotchi/update/:id' element={<UpdateTamagotchi />} />
              <Route path='/graveyard' element={<Graveyard />} />
            </Routes>
          </BrowserRouter>
        </div>
      </div>
    </div>

  );
}

export default App;
