import React from "react";
import { Navigate } from "react-router-dom";

import '../../css/users.css';

import { tamagotchiApiService } from "../../services/tamagotchiApiService";

export default class Users extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newUser: {
                username: null,
                nameTamagotchi: null
            },
            getUser: {
                username: null
            },
            check: null,
            userExist: null,
            inscripValid: false,
            redirect: false
        }
    }

    render() {
        return (
            <>
                <div className="users-container">
                    <div className="users-list-global">
                        <h1>Connexion :</h1>
                        
                        <ul className="users-list">
                            {this.state.userExist === false && <p style={{ color: 'red' }}>L'utilisateur n'existe pas.</p>}
                            
                            <input
                                id='getpseudo'
                                type="text"
                                placeholder="Pseudo"
                                onChange={element => this.setState({
                                    getUser: {
                                        ...this.state.getUser,
                                        username: element.target.value
                                    }
                                })}
                            />
                            
                            <button
                                type='submit'
                                onClick={(e) => this.connexionUser(e)}
                            >
                                Se connecter
                            </button>
                        </ul>
                    </div>
    
                    <div>
                        <h1>Ajouter un utilisateur :</h1>
                        
                        <form className="users-form">
                            {this.state.inscripValid && <p style={{ color: 'green' }}>Utilisateur créé ! Veuillez maintenant vous connecter avec votre pseudo.</p>}
                            {this.state.check === false && <p style={{ color: 'red' }}>Le pseudo existe déjà.</p>}
                            <input
                                id='pseudo'
                                type="text"
                                placeholder="Pseudo de votre utilisateur"
                                onChange={element => this.setState({
                                    newUser: {
                                        ...this.state.newUser,
                                        username: element.target.value
                                    }
                                })}
                            />
                            
                            <input
                                type="text"
                                placeholder="Nom de votre premier Tamagotchi"
                                onChange={element => this.setState({
                                    newUser: {
                                        ...this.state.newUser,
                                        nameTamagotchi: element.target.value
                                    }
                                })}
                            />
                            
                            <button
                                type='submit'
                                onClick={(e) => this.createUser(e)}
                            >
                                Créer
                            </button>
                        </form>
                    </div>
                </div>
                {this.state.redirect && <Navigate replace to="/tamagotchis" />}
            </>
        )
    }

    // Fonction qui crée un Utilisateur
    createUser = async (e) => {
        e.preventDefault();

        try {
            // Partie vérification du pseudo
            const URL = `/user/exist/`;

            let response = await tamagotchiApiService.request({
                url: URL,
                method: 'POST',
                data: {
                    username: this.state.newUser.username
                }
            });

            // Si le pseudo n'existe pas déjà dans la BDD
            if (response.data.data === false) {
                this.setState({ check: true });

                const URLSecond = `/user/`;
                let data = {
                    "username": this.state.newUser.username,
                    "tamagotchiName": this.state.newUser.nameTamagotchi
                };

                let response = await tamagotchiApiService.request({
                    url: URLSecond,
                    method: 'POST',
                    data: data
                });

                if (response.data.success) {
                    this.setState({ inscripValid: true });
                }
            } else {
                this.setState({ check: false });
            }        
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction qui connecte l'utilisateur
    connexionUser = async (e) => {
        e.preventDefault();

        try {
            const URL = `/user/search/`;

            let response = await tamagotchiApiService.request({
                url: URL,
                method: 'POST',
                data: {
                    "username": this.state.getUser.username
                }
            });

            if (response.data.data === "No results") {
                this.setState({ userExist: false });
            } else {
                this.setState({ userExist: true });
                localStorage.setItem('user_id', response.data.data.id);
                this.setState({ redirect: true });
            }
        } catch (error) {
            console.log(error);
        }
    }
}