import React from 'react'
import { Link } from 'react-router-dom';
import moment from 'moment';

import Tombstone from '../../components/tombstone';
import '../../css/graveyard.css';

import { tamagotchiApiService } from '../../services/tamagotchiApiService';

export default class Graveyard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tamagotchis: []
        }
    }

    async componentDidMount() {
        // Appel la route qui récupère la liste des Tamagotchis décédés
        try {
            const user_id = localStorage.getItem('user_id');
            const URL = `/user/tamagotchi/dead/${user_id}`;

            const deadTamagotchis = await tamagotchiApiService.request({
                url: URL,
                method: 'GET'
            });

            if (deadTamagotchis.data.success) {
                this.setState({ tamagotchis: deadTamagotchis.data.data });
            } else {
                console.log('error');
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <>
                <div className='graveyard-container'>
                    <div>
                        <Link to={{ pathname: '/tamagotchis' }}>← Retour aux Tamagotchis</Link>

                        <h1>Cimetière</h1>
                    </div>

                    <div className='graveyard-tombstones'>
                        {this.state.tamagotchis.length > 0 ?
                            this.state.tamagotchis.map((bestiole, index) =>
                            <Tombstone
                                key={index}
                                name={bestiole.life.name}
                                birthDate={moment(bestiole.life.birth).format('DD/MM/YYYY')}
                                deathDate={moment(bestiole.death).format('DD/MM/YYYY')}
                                level={bestiole.life.levels}
                            />
                        )
                        :
                        <p>Vous n'avez pas de Tamagotchi morts.</p>}
                    </div>
                </div>
            </>
        )
    }
}
