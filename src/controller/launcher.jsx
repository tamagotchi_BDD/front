import React, { useEffect } from "react";
import { Link } from "react-router-dom";

const Launcher = () => {
    useEffect(() => {
      document.title = "Tamagotchi"
      localStorage.setItem('user_id', null)
    });

    return (
        <>
            <Link className="Launcher-button" to={{ pathname: "/users" }}><h1>Start !</h1></Link>
        </>
    )
}

export default Launcher;