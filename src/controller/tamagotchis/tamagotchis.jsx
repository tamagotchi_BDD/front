import React from 'react';
import { Link } from 'react-router-dom';

import CardTamagotchi from '../../components/cardTamagotchi';
import '../../css/tamagotchis.css';

import { tamagotchiApiService } from '../../services/tamagotchiApiService';

export default class Tamagotchis extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tamagotchis: [],
            newTamagotchi: {
                name: null
            }
        }
    }

    async componentDidMount() {
        await this.getTamagotchi();
    }

    render() {
        return (
            <>
                <div className='tamagotchis-home'>
                    <div className='tamagotchis-list'>
                        <div className='flex'>
                            <Link to={{ pathname: '/' }}>← Déconnexion</Link>

                            <Link to={{ pathname: '/graveyard' }}>Aller au Cimetière →</Link>
                        </div>

                        <h1>Liste des Tamagotchis vivants :</h1>

                        <section>
                            {this.state.tamagotchis.length > 0 ?
                                this.state.tamagotchis.map((bestiole, index) =>
                                <Link
                                    key={index}
                                    to={{
                                        pathname: `/tamagotchi/update/${bestiole.life.id}`,
                                    }}
                                    state={{ tamagotchi: bestiole }}
                                >
                                    <CardTamagotchi
                                        name={bestiole.life.name}
                                        lvl={bestiole.life.levels}
                                        eat={bestiole.life.eat}
                                        drink={bestiole.life.drink}
                                        sleep={bestiole.life.bedtime}
                                        tired={bestiole.life.enjoy}
                                        user={bestiole.life.user_id}
                                    />
                                </Link>
                            )
                            :
                            <p style={{ textAlign: 'center' }}>Vous n'avez pas de Tamagotchi en vie.</p>}
                        </section>
                    </div>

                    <div>
                        <h1>Ajouter un Tamagotchi :</h1>

                        <form>
                            <input
                                id='name'
                                type='text'
                                placeholder='Nom du Tamagotchi'
                                onChange={element => this.setState({
                                    newTamagotchi: {
                                        ...this.state.newTamagotchi,
                                        name: element.target.value
                                    }
                                })}
                            />

                            <button
                                type='submit'
                                onClick={(e) => this.createTamagotchi(e)}
                            >
                                Créer
                            </button>
                        </form>
                    </div>
                </div>
            </>
        )
    }

    // Fonction qui récupère les Tamagotchi en vie
    getTamagotchi = async () => {
        try {
            const user_id = localStorage.getItem('user_id');
            const URL = `/user/tamagotchi/alive/${user_id}`;

            const aliveTamagotchis = await tamagotchiApiService.request({
                url: URL,
                method: 'GET'
            });

            if (aliveTamagotchis.data.success) {
                this.setState({ tamagotchis: aliveTamagotchis.data.data });
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction qui crée un Tamagotchi
    createTamagotchi = async (e) => {
        e.preventDefault();

        try {
            const user_id = localStorage.getItem('user_id');
            const URL = `/tamagotchi/`;

            let data = {
                "tamagotchiName": this.state.newTamagotchi.name,
                "id": user_id
            };

            let response = await tamagotchiApiService.request({
                url: URL,
                method: 'POST',
                data: data
            });

            if (response.data.success) {
                await this.getTamagotchi();
            }
        } catch (error) {
            console.log(error);
        }
    }
}
