import React from 'react';
import { Link, Navigate, useLocation, useParams } from 'react-router-dom';
import { Chart, BarElement, DoughnutController, CategoryScale, LinearScale, Tooltip, Legend, ArcElement } from 'chart.js';

import Modal from '../../components/modal';
import '../../css/tamagotchis.css';
import game from '../../img/gamepad-solid.svg';
import moon from '../../img/moon-solid.svg';
import mug from '../../img/mug-hot-solid.svg';
import ustensils from '../../img/utensils-solid.svg';

import { tamagotchiApiService } from '../../services/tamagotchiApiService';

class UpdateTamagotchi extends React.Component {
  constructor(props) {
    super(props);

    this.location = props.location;

    this.state = {
      updateTamagotchi: {
        id: this.location.state.tamagotchi.life.id,
        name: this.location.state.tamagotchi.life.name,
        levels: this.location.state.tamagotchi.life.levels,
        eat: this.location.state.tamagotchi.life.eat,
        drink: this.location.state.tamagotchi.life.drink,
        bedtime: this.location.state.tamagotchi.life.bedtime,
        enjoy: this.location.state.tamagotchi.life.enjoy
      },
      chart: null,
      modal: false
    }
  }

  componentDidMount() {
    this.refreshTamagotchi();

    this.deadTamagotchi();
  }

  render() {
    return (
      <>
        {this.state.modal && <Modal />}

        <div className='tamagotchis-home'>
          <div className='tamagotchis-list'>
            <Link to={{ pathname: '/tamagotchis' }} style={{ marginLeft: 20 }}>← Retour aux Tamagotchis</Link>

            <h1>S'occuper de {this.state.updateTamagotchi.name}</h1>

            <section>
              <div className='flex'>
                <button className='choice-action' onClick={() => this.eatTamagotchi()}>
                  <img src={ustensils} alt="Icône pour nourrir" />

                  <p>{this.state.updateTamagotchi.eat}/100</p>

                  <p><b>Nourrir</b></p>
                </button>

                <button className='choice-action' onClick={() => this.drinkTamagotchi()}>
                  <img src={mug} alt="Icône pour boire" />

                  <p>{this.state.updateTamagotchi.drink}/100</p>

                  <p><b>Boire</b></p>
                </button>

                <button className='choice-action' onClick={() => this.sleepTamagotchi()}>
                  <img src={moon} alt="Icône pour coucher" />

                  <p>{this.state.updateTamagotchi.bedtime}/100</p>

                  <p><b>Coucher</b></p>
                </button>

                <button className='choice-action' onClick={() => this.gameTamagotchi()}>
                  <img src={game} alt="Icône pour jouer" />

                  <p>{this.state.updateTamagotchi.enjoy}/100</p>

                  <p><b>Jouer</b></p>
                </button>
              </div>
            </section>

            <canvas id="chart"></canvas>
          </div>
        </div>
        {this.state.redirect && <Navigate replace to={`/tamagotchis`}/>}
      </>
    )
  }

  // Fonction qui donne à manger au Tamagotchi
  eatTamagotchi = async () => {
    try {
      const URL = `/tamagotchi/eat/`;
      let data = {
        "id": this.state.updateTamagotchi.id
      };

      let response = await tamagotchiApiService.request({
        url: URL,
        method: 'POST',
        data: data
      });

      if (response.data.success) {
        await this.refreshTamagotchi()
      }
    } catch (error) {
      console.log(error);
    }
  }

  // Fonction qui donne à boire au Tamagotchi
  drinkTamagotchi = async () => {
    try {
      const URL = `/tamagotchi/drink/`;
      let data = {
        "id": this.state.updateTamagotchi.id
      };

      let response = await tamagotchiApiService.request({
        url: URL,
        method: 'POST',
        data: data
      });

      if (response.data.success) {
        await this.refreshTamagotchi()
      }
    } catch (error) {
      console.log(error);
    }
  }

  // Fonction qui couche le Tamagotchi
  sleepTamagotchi = async () => {
    try {
      const URL = `/tamagotchi/bedtime/`;
      let data = {
        "id": this.state.updateTamagotchi.id
      };

      let response = await tamagotchiApiService.request({
        url: URL,
        method: 'POST',
        data: data
      });

      if (response.data.success) {
        await this.refreshTamagotchi()
      }
    } catch (error) {
      console.log(error);
    }
  }

  // Fonction qui joue avec le Tamagotchi
  gameTamagotchi = async () => {
    try {
      const URL = `/tamagotchi/enjoy/`;
      let data = {
        "id": this.state.updateTamagotchi.id
      };

      let response = await tamagotchiApiService.request({
        url: URL,
        method: 'POST',
        data: data
      });

      if (response.data.success) {
        await this.refreshTamagotchi()
      }
    } catch (error) {
      console.log(error);
    }
  }

  // Fonction qui actualise la page
  refreshTamagotchi = async () => {
    const URL = `/tamagotchi/${this.state.updateTamagotchi.id}`;

    let response = await tamagotchiApiService.request({
      url: URL,
      method: 'GET',
    });

    this.setState({ updateTamagotchi: response.data.data });

    this.initChart();

    // Fonction qui fait apparaître une popup lorsque le Tamagotchi est mort
    if (response.data.data.eat === 0 || response.data.data.drink === 0 || response.data.data.bedtime === 0 || response.data.data.enjoy === 0) {
      this.setState({ modal: true });
    }
  }

  // Fonction qui crée un diagramme de statistiques de la santé du Tamagotchi
  initChart = () => {
    if(this.state.chart !== null) this.state.chart.destroy();

    const ctx = document.getElementById('chart').getContext('2d');
    Chart.register(BarElement,DoughnutController,CategoryScale,LinearScale,Tooltip,Legend,ArcElement)
    const chart = new Chart(ctx, {
      type: 'doughnut',
      data: {
        labels: [
          'Nourrir',
          'Boire',
          'Coucher',
          'Jouer'
        ],
        datasets: [{
          label: 'Statistics',
          backgroundColor: [
            'rgb(174, 137, 100)',
            'rgb(135, 206, 250)',
            'rgb(102, 0, 153)',
            'rgb(60, 179, 113)'
          ],
          borderColor: [
            'rgb(139, 108, 66)',
            'rgb(0, 191, 255)',
            'rgb(75, 0, 130)',
            'rgb(46, 139, 87)'
          ],
          data: [
            this.state.updateTamagotchi.eat,
            this.state.updateTamagotchi.drink,
            this.state.updateTamagotchi.bedtime,
            this.state.updateTamagotchi.enjoy
          ],
        }]
      },
      options: {
        responsive: true,
        plugins: {
          legend: {
            position: 'top',
          },
          title: {
            display: true,
            text: 'Chart.js Doughnut Chart'
          }
        }
      }
    })
    this.setState({'chart': chart});
  }

  // Fonction qui fait apparaître une popup lorsque le Tamagotchi est mort
  deadTamagotchi = () => {
    if (this.state.updateTamagotchi.eat === 0 || this.state.updateTamagotchi.drink === 0 || this.state.updateTamagotchi.bedtime === 0 || this.state.updateTamagotchi.enjoy === 0) {
      this.setState({ modal: true });
    }
  }
}

export default () => (
  <UpdateTamagotchi params={useParams()} location={useLocation()}/>
);
