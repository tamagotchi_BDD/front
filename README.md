<h1 style="border-bottom: 1px solid grey"> Tamagotchi </h1>


<h2 style="border-bottom: 1px solid grey"> 1- Installation </h2>

<br>

Télécharger le projet : https://gitlab.com/tamagotchi_BDD/front/-/archive/master/front-master.zip

```
git clone git@gitlab.com:tamagotchi_BDD/front.git
ou
git clone https://gitlab.com/tamagotchi_BDD/front.git
```

Ensuite, installer les dépendances 

```
npm i 
```

Puis il faut configurer le fichier d'environnement. Vous pouvez renommer '.env.sample' en '.env' puis configurer les variables

```
PORT=port de l'application

REACT_APP_API_PROTOCOL=http
REACT_APP_API_HOST=adresse ip ou url du serveur back (ex : 127.0.0.1 sans http)
REACT_APP_API_PORT=port du serveur back
REACT_APP_API_ROOT=/api
```

Finalement, 

```
npm start
```

